var CategoryApp = (function(){

    var _categoryData = {};
    var _init = function(){
        _getData();
    };

    var _getData = function(){
        $.getJSON("data/data.JSON", function(data){
            _categoryData = data.categories;
            console.log('hleo ', _categoryData);

            $.each(_categoryData, function(idx, category){
                var mHTML = '<section id="' + category.id +'" class="cat"><img src="'+ category.itemImage +'"><p>'+ category.itemName +'</p></section>';
                if(category.categoryName == "Toys"){
                    $('.toy').append(mHTML);
                }else if(category.categoryName == "Home") {
                    $('.home').append(mHTML);
                }else if(category.categoryName == "Clothes"){
                    $('.clothes').append(mHTML);
                }else if(category.categoryName == "Jewelry"){
                    $('.jewelry').append(mHTML);
                }
            });

            _setBindings();
        });


    };

    var _setBindings = function(){
        $(".cat").click(function(e){
            $(".loader").css("display", "flex");
            var currentCategoryID = $(this).attr('id');

            $.each(_categoryData, function(idx, cat){
                if(currentCategoryID == cat.id){
                    var det = '<h2 class="itemName">' + cat.itemName + '</h2><p class="itemPrice">' + cat.itemPrice + '</p><p class="itemDescription">' + cat.itemDescription  + '</p><img src="' + cat.itemImage + '">';
                    $(".itemDetails").html(det);
                    _moveDetailPageOn();
                }
            })
        });

        $(".backBtn").click(function(e) {
            $(".categoryDetails").html("");
            $('.details').transition({ x: '100vw' }, 250, "linear");
        });

    };

    var _moveDetailPageOn = function(){
        $('.details').transition({ x: '-100vw' }, 250, "snap");
        $(".loader").css("display", "none");
    };

    return {
        init: _init
    }
})();
